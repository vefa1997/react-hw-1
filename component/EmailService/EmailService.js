import React, {useState} from 'react'
import AuthInput from "./AuthInput";
import Message from "./Message";

const EmailService = props => {
  const submitBtn = React.createRef();
  const [state, setState] = useState({
isCorrectEmail:false,
isCorrectPassword: false,
  isValidationPassed:() => this.isCorrectEmail && this.isCorrectPassword});

  const validateEmail = (e) => {
      const allowedEmailServices = props.allowedEmailServices;
  };
  const  validatePassword = (e) => {} ;
  const submitFormHandler = () => {};
  const successMessage = <Message text={'Hello! you entered the empty email client. ' } />;



  return(
      <form onSubmit={submitFormHandler}>
          <AuthInput type={'email'} validation ={validateEmail}/>
          <AuthInput type={'password'} validation={validatePassword}/>
          <input disabled ref={submitBtn} type="submit" value={'Log in'}/>
      </form>
          );
};
export default EmailService;