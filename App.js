import React, {Component, useState} from 'react';
import ModalButton from "./component/Modal/ModalButton";
import ModalWindow from "./component/Modal/ModalWindow";
import "./App.css"
function App() {
   const  [modal_First,setfirstStatus]=useState(false);
   const  [modal_Second,setsecondStatus]=useState(false);
   const firstModalButton =()=>setfirstStatus(a => !a)
   const secondModalButton =()=>setsecondStatus(b => !b)
   return (
       <div className="ToggleModal">
          <ModalButton backgroundColor="red" text="Open first modal" onClick={firstModalButton}/>
          <ModalButton backgroundColor="blue" text="Open second modal" onClick={secondModalButton}/>
          {modal_First && (
              <ModalButton header="Do you want remove this file?"
                           CloseIcon={true}
                           text="Are you sure you want delete it?"
                           CloseButton={firstModalButton}
                           movement={[
                              <ModalButton
                                  key={1}
                                  backgroundColor="green"
                                  text="OK"
                                  onClick={firstModalButton}/>,
                              <ModalButton
                                  key={1}
                                  backgroundColor="green"
                                  text="CANCEL"
                                  onClick={firstModalButton}/>
                           ]}/>)}
          {modal_Second &&
          (
              <ModalButton header="Do you want remove this file?"
                           CloseIcon={true}
                           text="Are you sure you want delete it?"
                           CloseButton={secondModalButton}
                           movement={[
                              <ModalButton
                                  key={1}
                                  backgroundColor="green"
                                  text="OK"
                                  onClick={secondModalButton}/>,
                              <ModalButton
                                  key={1}
                                  backgroundColor="green"
                                  text="CANCEL"
                                  onClick={secondModalButton}/>
                           ]}/>)
          }
       </div>
   );
}
export default App;